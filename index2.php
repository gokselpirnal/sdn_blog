<?php
include "inc/config.php";
?>
<html>
<head>
    <link rel="stylesheet" href="assets/stylesheets/custom.css">
    <script type="application/javascript" src="https://code.jquery.com/jquery-1.11.3.js"></script>
    <script type="application/javascript" src="assets/js/app.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <?php echo "<h1>Uygulama Anasayfası</h1>"; ?>

    <h3>POSTS Tablosunun Listeleme Örneği</h3>
    <p>
        <i>$posts = DB::query("SELECT * FROM posts");</i>
        <br>
        şeklinde kullanılabilir.
    </p>
    <div class="view_posts_list">
        <table class="table">
            <thead><td>ID</td><td>TITLE</td><td>CONTENT</td><td>CATEGORY</td></thead>
            <?php $posts = DB::query("SELECT * FROM posts");
                foreach($posts as $post) {
                    echo "<tr><td>".$post->id."</td><td>".$post->title."</td><td>".$post->content."</td><td>".$post->category_id."</td></tr>";
                }
            ?>
        </table>
    </div>

    <h3>POST Tablosuna Yeni Veri Ekleme Örneği</h3>
    <p>
        <strong>inc</strong> klasörü altındaki <i>insert.php</i> dosyasına POST isteği gönderilip orada karşılanacak.
        <br>
        Diğer işlemler için de bu formata sadık kalınmalı.
    </p>
    <div class="insert_new_post">
        <form action="inc/insert.php" method="post">
            <div class="form-group">
                <label for="title">TITLE</label>
                <input type="text" name="title" class="form-control" id="title" placeholder="TITLE">
            </div>
            <div class="form-group">
                <label for="title">CATEGORY_ID</label>
                <input type="text" name="category" class="form-control" id="title" placeholder="category_id">
            </div>
            <div class="form-group">
                <label for="title">USER_ID</label>
                <input type="text" name="user" class="form-control" id="title" placeholder="user_id">
            </div>
            <div class="form-group">
                <label for="title">CONTENT</label>
                <textarea class="form-control" name="content" rows="3"></textarea>
            </div>
            <input type="submit" class="btn btn-default" value="EKLE"/>
        </form>
    </div>

</div>
</body>
</html>