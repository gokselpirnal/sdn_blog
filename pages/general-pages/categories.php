<?php
/**
 * Created by PhpStorm.
 * User: gülçin
 * Date: 01.07.2015
 * Time: 13:26
 * List all categories
 */

$pageTitle="Kategoriler";
$categorylist = DB::query("SELECT * FROM categories");
?>

<ul class="list-group">
    <?php
    foreach($categorylist as $category) {
        echo "<li class='col-lg-12 list-group-item'><a href='kategoriler/".$category->id."-".create_slug($category->name)."'><i class='fa fa-check-square' style='width:20px;'></i> ".$category->name."</a></li>";
    }
    ?>
</ul>

