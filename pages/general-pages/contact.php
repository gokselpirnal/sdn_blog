<?php
/**
 * Created by PhpStorm.
 * User: gülçin
 * Date: 1.7.2015
 * Time: 14:04
 */
$pageTitle="İletişim";
?>

<form>
    <div class="form-group">
        <label for="Name">Ad</label>
        <input type="text" class="form-control" id="name" placeholder="Ad">
    </div>
    <div class="form-group">
        <label for="">Soyad</label>
        <input type="text" class="form-control" id="surname" placeholder="Soyad">
    </div>
    <div class="form-group">
        <label for="Comment">Mesaj</label>
        <textarea class="form-control" rows="4" placeholder="Mesaj"></textarea>
    </div>

    <div class="form-group">
    <input class="btn btn-default pull-right" style="margin-top: 15px" type="button" value="Gönder">
    </div>
</form>