<?php
/**
 * Created by PhpStorm.
 * User: Umut
 * Date: 01.07.2015
 * Time: 13:21
 * Default index content
 */

$pageTitle = "Ana Sayfa";
$posts = DB::query("SELECT * FROM posts");

?>

<?php
    foreach($posts as $post)
    {
?>
    <h2>
        <a href="makale/<?php echo $post->id."-".$post->slug; ?>"><?php echo $post->title; ?></a>
    </h2>
    <p class="lead">
        <b><?php echo getUserFromPost($post->id); ?></b> tarafından
    </p>
    <p><span class="glyphicon glyphicon-time"></span> Oluşturuldu: <?php echo $post->created_date; ?></p>
    <p><span class="glyphicon glyphicon-edit"></span> Güncellendi: <?php echo $post->updated_date; ?></p>
    <hr>
    <img class="img-rounded img-thumbnail" src="<?php echo getImageFromPost($post->id); ?>">
    <hr>
    <p><?php echo shorterString($post->content,250); ?></p>
    <a class="btn btn-primary" href="makale/<?php echo $post->id."-".$post->slug; ?>">Devamını Oku <span class="glyphicon glyphicon-chevron-right"></span></a>
    <hr>
    <?php
    }
?>

<!-- Pager -->
<ul class="pager">
    <li class="previous">
        <a href="#">&larr; Older</a>
    </li>
    <li class="next">
        <a href="#">Newer &rarr;</a>
    </li>
</ul>