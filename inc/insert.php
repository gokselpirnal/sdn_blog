<?php

// Veritabanı bağlantıları dahil edildi.
include "config.php";

if($_POST) {
    $title = $_POST["title"];
    $content = $_POST["content"];
    $category = $_POST["category"];
    $user = $_POST["user"];

    $id = DB::insert(
        'INSERT INTO posts (title, content, category_id, user_id) VALUES(?, ?, ?, ?)',
        array($title, $content, $category, $user)
    );

    if($error = DB::getLastError())
    {
        echo 'Bir hata oldu, hata mesajı: ' . $error[2];
    }
    else
    {
        echo 'Başarıyla eklendi! Eklenen ID ' . $id;
        echo "<br><a href='../index2.php'>Anasayfa'ya Dönün</a>";
    }



}