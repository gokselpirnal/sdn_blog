<?php

function create_slug($string)
{
    $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    return $slug;
}

function getPostCountFromCategory($categoryID)
{
    $posts = DB::query("SELECT * FROM posts where category_id=".$categoryID);
    return $posts->rowCount();
}

function getUserFromPost($postID)
{
    $user = DB::getRow("SELECT * FROM posts as p inner join users as u on p.user_id=u.id where p.id =".$postID);
    return $user->username;
}

function getImageFromPost($post_id){
    $image= DB::getRow("SELECT * FROM posts as p inner join images as i on p.image_id=i.id where p.id=".$post_id);
    return $image->url;
}
function shorterString($string, $str = 250)
{
    if (strlen($string) > $str)
    {
        if (function_exists("mb_substr")) $string = mb_substr($string, 0, $str, "UTF-8").'...';
        else $string = substr($string, 0, $str).'...';
    }
    return $string;
}

?>