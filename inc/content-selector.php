<?php
/**
 * Created by PhpStorm.
 * User: Umut
 * Date: 01.07.2015
 * Time: 10:53
 * Change content from get data
 */

if(isset($_GET['kategori']))
{
    $fullLink = $_GET['kategori'];
    $parts = explode("-", $fullLink);
    $id = $parts[0];
    include "pages/single-category.php";
}
else if(isset($_GET['makale']))
{
    $fullLink = $_GET['makale'];
    $parts = explode("-", $fullLink);
    $id = $parts[0];
    include "pages/single-article.php";
}
else if(isset($_GET['genel']))
{
    $pageName = $_GET['genel'];
    include "pages/general-page.php";
}
else
{
    include "pages/index.php";
}

?>